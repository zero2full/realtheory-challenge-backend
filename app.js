const express = require('express');
const mongoose = require('mongoose');
const recordsRouter = require('./router/records-router');
const userRouter = require('./router/user-router');

const app = express();

mongoose.connect(
  `mongodb+srv://realtheory:${process.env.MONGO_DB_PW}@realtheory.yw4vz.mongodb.net/realtheory-challenge?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }
).then(() => {
  console.log('Connection is successful!')
}).catch(err => {
  console.log('Connection is failed.')
});

app.use(express.json());

//set heads for Cross-Origin Resource Sharing (CORS)
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Oringin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, PUT, DELETE, OPTIONS'
  );
  next();
});

app.use('/api/records', recordsRouter);
app.use('/api/user', userRouter);

app.listen(4000, () => {
  console.log('Server is starting...');
});
