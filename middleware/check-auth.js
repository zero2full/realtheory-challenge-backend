const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        // get token information from req
        const token = req.headers.authorization.split(' ')[1];
        // verificate token information
        jwt.verify(token, process.env.JWT_KEY)
        next();
    } catch(error) {
        res.status(401).send({message: 'Auth is failed'});
    }
}