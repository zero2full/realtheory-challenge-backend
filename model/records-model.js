const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recordsSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name is required.']
    },
    studentID: {
        type: String,
        required: [true, 'Student ID is required.']
    },
    grade: {
        type: String,
        required: [true, 'Grade is required.']
    }
});

module.exports = mongoose.model('Record', recordsSchema);
