const express = require('express');
const router = express.Router();
const RecordsModel = require('../model/records-model');
const checkAuth = require('../middleware/check-auth');

// get API
router.get('', (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const pageIndex = +req.query.pageindex;
  const Records = RecordsModel.find();
  let resRecords;
  
  // set paginator
  if ((pageIndex === 0 || !!pageIndex) && pageSize) {
    Records.limit(pageSize).skip(pageSize * pageIndex);
  }
  
  Records.then(records => {
    resRecords = records;
    // count records number
    return RecordsModel.countDocuments();
  }).then(recordsNum => {
    res.status(200).send({
      records: resRecords,
      recordsNum
    })
  })
  .catch(err => {
    res.status(500).send({error: err.message});
  });
});

// fetch specific record API
router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  RecordsModel.findById({_id: id}).then(record => {
    if (!record) {
      return res.status(404).send({message: 'no student record found'})
    }
    res.status(200).send(record);
  }).catch(err => {
    res.status(500).send({error: err.message});
  })
})

// POST API
router.post('', checkAuth, (req, res, next) => {
  const newRecord = {
    name: req.body.name,
    studentID: req.body.studentID,
    grade: req.body.grade
  }
  RecordsModel.create(newRecord)
    .then((record) => {
      res.status(201).send(record);
    })
    .catch(err => {
      res.status(500).send({error: err.message});
    });
});

// UPDATE API
router.put('/:id', checkAuth, (req, res, next) => {
  RecordsModel.findByIdAndUpdate({ _id: req.params.id }, { ...req.body })
    .then(() => {
      return RecordsModel.findById({ _id: req.params.id });
    })
    .then((updatedRecord) => {
      res.status(200).send(updatedRecord);
    })
    .catch(err => {
      res.status(500).send({error: err.message});
    });
});

// DELETE API
router.delete('/:id', checkAuth, (req, res, next) => {
  RecordsModel.findByIdAndRemove({ _id: req.params.id })
    .then((record) => {
      res.status(200).send(record);
    })
    .catch(err => {
      res.status(500).send({error: err.message});
    });
});

module.exports = router;
