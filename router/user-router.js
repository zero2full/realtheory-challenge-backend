const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const router = express.Router();

const UserModel = require('../model/user-model');

// sign up API
router.post('/signup', (req, res, next) => {
  // hash password
  bcrypt
    .hash(req.body.password, 10)
    .then((hashedPw) => {
      // return create recore promise
      return UserModel.create({ email: req.body.email, password: hashedPw });
    })
    .then((resData) => {
      // set up JsonWebToken
      const token = jwt.sign(
        { email: resData.email, userID: resData._id },
        process.env.JWT_KEY,
        { expiresIn: '1h' }
        );
        
        res.status(201).send({ token, expiresIn: 3600*1000});
      })
      .catch(err => {
        console.log(err);
      res.status(500).send({error: err.message});
    });
});

// login API
router.post('/login', (req, res, next) => {
  let authUser;
  // check whether user name exists
  UserModel.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        return res.status(401).send({ message: 'Auth is failed' });
      }
      authUser = user;

      // checke whether password is correct
      bcrypt.compare(req.body.password, user.password)
      .then((result) => {
        if (!result) {
          return res.status(401).send({ message: 'Auth is failed' });
        }

        // set up the JsonWebToken
        const token = jwt.sign(
          { email: authUser.email, userID: authUser._id },
          process.env.JWT_KEY,
          { expiresIn: '1h' }
        );

        res.status(200).send({ token, expiresIn: 3600*1000});
      });
    })
    .catch(err => {
      res.status(500).send({error: err.message});
    });
});

module.exports = router;
